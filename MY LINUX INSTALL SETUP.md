# 1. Nextcloud Desktop Client
 Nextcloud Desktop Client is a tool to synchronize files from Nextcloud Server with your computer.
 I use Nextcloud to sync my files, mostly those which concerned to my college assignments and crucial tasks.
 I use https://woelkli.com/ server to store my files in.
---
sudo dnf install nextcloud-client
---
 Then log in using your account
 If you have problem such as getting logged out everytime after you reboot your machine I suggest you to install 
---
sudo dnf install libgnome-keyring
---

# 2. Telegram Desktop
 I use Telegram as my prefered personal messenger app. I also occasionally use it to sync files between my phone and my laptop or other devices
---
sudo dnf install telegram-desktop
---
 Then log in using your mobile phone number

# 3. Firefox 
 The best browser ever. I dont think I have to explain why.
---
sudo dnf install firefox
---
 After that you might want to install some extensions that I find very useful, they can made a huge difference in your browsing experiences.
 Extensions List:  
 a. Bitwarden Password Manager <<< THIS ONE IS REALLY IMPORTANT, ALL MY ONLINE ACCOUNTS IS IN HERE >>>
 b. Gnome Shell Integration(it depends whether you use gnome or not)
 c. Reddit Enhancement Suite
 d. uBlock Origin
 e. Vimium
 f. NoScript  
 To further enhance your browsing experience you might want to edit your 'hosts' file to unblock some blocked sites. (because Kominfo -sigh)  
 You just need to get the list of sites and their corresponding ip(s) from:
---
https://github.com/gvoze32/unblockhostid
---
 After you got the copy you only need to paste that list into your 'hosts' file. DONE...congrats, now you can do scientific research!!!

# 4. Libre Office Suite
 Because you are going to need it. Believe me, no matter how much you prefer microsoft office, you dont have that choiche in Linux.. 
 It is not the end of the world!!! I find them actually better sometimes man.
---
sudo dnf install libreoffice
---

# 5. MPV Media Player
 The best media player, EVER!!! I find myself constantly using mpv to play any media files from audio, song, to video. 
 However i still have VlC installed on my machine too. While MPV provides performance and simplicity, VLC provides me with modularity and intricacy
---
sudo dnf install mpv
---

# 6. VLC Media Player
 This is also self explanatory, they are great, and open source(duh). I like them a lot. They can convert files, and even stream online videos in simple steps.
---
sudo dnf install vlc
---

# 7. Modem Manager GUI
 I still use my modem for internet connection sometimes when in emergency. This tool provides a nice graphical interface to set up my modem connection.
---
sudo dnf install modem-manager-gui
---

# 8. Oracle VM VirtualBox
 Virtual Machine that use besides KVM/QEMU
 Because Virtualbox is not included in the repository of many linux distro, we have to install it using their own repository(for auto update features)
## Download the repository
---
https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
---
## Copy 'virtualbox.repo' file into /etc/yum.repos.d/ and then
---
sudo dnf install virtualbox
---

# 9. Gmone power Statistics <<<SPECIFIC TO GNOME DESKTOP ENVIRONMENT. if you're running other DE, skip this>>>
---
sudo dnf install gnome-power-manager
---

# 10. PulseEffects
 This program can tweak the quality of sound production in your laptop, especially in my ThinkPad x230 which has a really poor speaker. 
---
sudo dnf install pulseeffects
---
 I have also included my configuration in this folder under the name 'PulseEffects'  
 Copy the whole folder to your PulseEffects config folder  
 The location of that directory depends on how you installed PulseEffects.  
 If you installed it through Flatpak, you can find it in ~/.var/app/com.github.wwmm.pulseeffects/config/PulseEffects,   
 or it should normally be in ~/.config/PulseEffects  

# 11. Sublime text editor
 I rarely use it but it is nice to have
 Install the GPG key: 
---
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
---
 I use the dev release channel so this is what I run
---
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/dev/x86_64/sublime-text.repo  
sudo dnf install sublime-text
---

# 12. TeamViewer
 Useful to remote my desktop(I don't have a working monitor at the moment),  
 for gaming purpose I use Moonlight in Windows but since my graphic card went apeshit it has been a difficut situation for me
---
wget https://download.teamviewer.com/download/linux/teamviewer.x86_64.rpm  
sudo dnf -y install teamviewer.x86_64.rpm
---

# 13. KVM/QEMU
 Great alternative to Virtualbox. KVM/QEMU has less overhaed so it can achieve better performace
---
sudo dnf -y install bridge-utils libvirt virt-install qemu-kvm
---
 After installation, verify that Kernel modules are loaded
---
lsmod | grep kvm
---
 Also install useful tools for virtual machine management.
---
sudo dnf -y install virt-top libguestfs-tools
---
 By default, KVM daemon 'libvirtd' is not started, start the service using the command:
---
sudo systemctl start libvirtd  
sudo systemctl enable libvirtd
---
 If you have a Desktop version of Fedora, you can install virt-manager which gives users a GUI interface to manage your Virtual Machines.
---
sudo dnf -y install virt-manager
---

# 14.  X2Go Client/Server<<<Depends on which direction you want to use it>>>
 An X forwarder, I use it to remote my desktop quite often. Only works with some DE. For the client do the following command
---
sudo dnf install x2goclient 
---
 For the server you have to do the following command
---
sudo dnf install x2goserver
---

 
